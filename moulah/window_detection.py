import wmi
import win32con
import win32api
import win32gui
import win32process

from moulah.settings import Rooms

__windows = []


def enumWindowsProc(hwnd, lParam):
    if (lParam is None) or ((lParam is not None) and (win32process.GetWindowThreadProcessId(hwnd)[1] == lParam)):
        text = win32gui.GetWindowText(hwnd)
        if text:
            wStyle = win32api.GetWindowLong(hwnd, win32con.GWL_STYLE)
            if wStyle & win32con.WS_VISIBLE:
                __windows.append((hwnd, text))


def enumProcWnds(pid=None):
    __windows = []
    win32gui.EnumWindows(enumWindowsProc, pid)


def pid_lookup(processes):
    pids = []
    for p in processes:
        for r in Rooms:
            if p[1] == Rooms[r]['process_name']:
                pids.append((p[0], r))
    return pids


def exclude_keywords(windows, room):
    filtered = windows.copy()
    for win in windows:
        for string in Rooms[room]['exclude']:
            if string in win[1]:
                filtered.remove(win)
    return filtered


def get_running_poker_rooms():
    running = {}
    c = wmi.WMI()
    processes = [(d.ProcessId, d.Name) for d in c.Win32_Process()]
    pids = pid_lookup(processes)
    for proc in pids:
        enumProcWnds(proc[0])
        running[proc[1]] = exclude_keywords(__windows.copy(), proc[1])
    return running